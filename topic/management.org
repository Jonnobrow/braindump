#+TITLE: Management
#+DATE: <2020-10-06 Tue 11:40>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: management
#+ROAM_TAGS: Management

- [[file:../notes/2020-10-06--10-42-19Z--definition_of_management.org][Definition of Management]]
- [[file:../notes/2020-10-06--10-45-36Z--what_makes_an_organisation_successful.org][What makes an organisation successful?]]
- [[file:../notes/2020-10-06--10-47-48Z--models_of_management.org][Models of Management]]
