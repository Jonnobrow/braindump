#+TITLE: Classification
#+DATE: <2020-10-06 Tue 12:16>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: classification
#+ROAM_TAGS: Classification Machine_Learning

- We can call 0 the "negative class" (e.g. benign tumor)
- We can call 1 the "positive class" (e.g. malignant)
- An algorithm like linear regression will not be suitable because it tries to fit to outliers which skew the classification

* [[file:../notes/2020-10-06--12-36-59Z--multiclass_classification.org][Multiclass Classification]]
* [[file:logistic_regression.org][Logistic Regression]]
