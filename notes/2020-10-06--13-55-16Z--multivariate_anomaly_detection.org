#+TITLE: Multivariate Anomaly Detection
#+DATE: <2020-10-06 Tue 14:55>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: multivariate_anomaly_detection
* Anomaly Detection using Multivariate [[file:2020-10-06--13-53-31Z--gaussian_distribution.org][Gaussian Distribution]]
- Given training set \(\{x^{(1)},x^{(2)},...\}\)
  - \(\mu = \frac{1}{m} \sum_{i=1}^{m}x^{(i)}\)
  - \(\Sigma = \frac{1}{m} \sum_{i=1}^{m}(x^{(i)} - \mu)(x^{(i)} - \mu)^{T}\)
- We can represent multivariate gaussian distributions that are axis aligned with the original model
- When to use:
  - Automatically capture correlations between features (original model will work fine if you create additional features)
  - When computation limits are large (computationally more expensive than the original model)
  - When \(m > n\) otherwise \Sigma is non-invertible
