#+TITLE: Debugging a Learning Algorithm
#+DATE: <2020-10-06 Tue 14:21>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: debugging_a_learning_algorithm
#+ROAM_TAGS: Machine_Learning

Descriptions of [[file:2020-10-06--13-02-08Z--bias_vs_variance.org][Bias and Variance]]

- Getting more training example: Fixes high variance
- Trying a smaller set of features: Fixes high variance
- Trying to add additional features: Fixes high bias
- Adding polynomial features: Fixes high bias
- Try decreasing \(\lambda\): Fixes high bias
- Try increasing \(\lambda\): Fixes high variance
