#+TITLE: Systematic Decision Making
#+DATE: <2020-10-06 Tue 11:55>
#+SETUPFILE: ~/.config/emacs/org-templates/roam-note.org
#+HUGO_SLUG: systematic_decision_making
#+ROAM_TAGS: Decision_Making COMP3219

* Benefits
- Best to be scientific or systematic about major decisions
- Thinking slowly is a good way to avoid bias
