#+TITLE: Principal Component Analysis - Algorithm
#+DATE: <2020-10-06 Tue 14:46>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: principal_component_analysis_algorithm
- Preprocess by carrying out mean normalisation and feature scaling
- To reduce data from n-dimensions to k-dimensions
  - Compute the "covariance matrix":
    - \(\Sigma = \frac{1}{m}\sum_{i=1}^{n}(x^{(i)})(x^{(i)})^T\)
    - Sigma is an \(n \times n\) matrix
    - ~Sigma = (1/m) * X' * X;~ in octave vectorised
  - Compute "eigenvectors" of matrix \(\Sigma\)
    - ~[U, S, V] = svd(Sigma);~
    - ~[U, S, V] = eig(Sigma);~
    - The U matrix will be an \(n \times n\) matrix
      - The columns of which are our \(u^{(i)}\) vectors
  - Define \(z = U_{\text{reduce}}^T X\)
    - Where \(U_{\text{reduce}}\) is the first \(k\) columns of the U matrix
    - And \(X\) is the training set in rows
