#+TITLE: Applications of Neural networks
#+DATE: <2020-10-06 Tue 13:43>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: applications_of_neural_networks
#+ROAM_TAGS: Neural_Networks

- Writing a truth table for variables can show you that we can build logical functions like AND, OR, XOR
- Use one-hot vectors on n-dimensions for an n-class classification problem
