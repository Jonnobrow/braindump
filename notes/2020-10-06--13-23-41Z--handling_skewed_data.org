#+TITLE: Handling Skewed Data
#+DATE: <2020-10-06 Tue 14:23>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: handling_skewed_data
#+ROAM_TAGS: Machine_Learning

* Handling Skewed Data
- We may have a lot more examples from one class than another
- *Precision/Recall*:
  - Precision classifier is the number of true positives divided by the number predicted positives
  - Recall is defined as the number of true positives divided by the number of actual positives
- Aim to maximise precision and recall which helps to counter skewed data
- Trade Off:
  - If we want to predict y=1 (cancer) only if very confident then: Higher Precision, lower recall
  - Suppose we want to avoid missing too many cases of cancer (avoid false negatives): Higher recall, lower precision
- F_1 Score: \(2 \times \frac{PR}{P + R}\)
