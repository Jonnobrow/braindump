#+TITLE: Representing the Hypothesis in Logisitic Regression
#+DATE: <2020-10-06 Tue 12:18>
#+SETUPFILE: ~/.config/emacs/org-templates/roam-note.org
#+HUGO_SLUG: representing_the_hypothesis_in_logistic_regression
#+ROAM_TAGS: Hypothesis Logisitic_Regression
* Representing the hypothesis
- Redefine the hypothesis function as:
  - \(h_{\theta}(x) = g(\theta^T x)\)
  - Where g is logistic function, in this case, the sigmoid function \(g(z) = \frac{1}{1+e^{-z}}\)
- The logistic function has a range between 0 and 1
- We can treat the output of the hypothesis function as the probability that \(y=1\) on input \(x\)
  - Therefore we can treat anything over 0.5 as 1 and 0 otherwise, hence classifying
* Simplified Cost Function and Gradient Descent
#+caption: Equivalent One Line Cost Function for Logisitic Regression
#+attr_latex: :placement [H]
#+label: fig:logisitic-regression-cost-oneline
#+begin_export latex
\begin{equation}
\text{Cost}(h_{\theta}(x),y) = -y log(h_{\theta}(x)) - (1-y)log(1-h_{\theta}(x))
\end{equation}
#+end_export
- We want to minimize our cost function and we use gradient descent to do so
- We repeatedly, simultaneously update all \(\theta_j\)
  - \(\theta_j := \theta_j - \frac{\alpha}{m} \sum_{i=1}^{m}(h_{\theta}(x^{(i)}) - y^{(i)})x_j^{(i)}\)
- *Reminder*: Check that the cost value against iterations decreases each iteration

#+caption: Vectorized Logisitic Regression Equations
#+attr_latex: :placement [H]
#+label: fig:vector-log-reg-eq
#+begin_export latex
\begin{align*}
g(z) &= \frac{1}{1 + e^{-z}} && \text{The sigmoid function}\\
h &= g(X \theta) && \text{The hypothesis matrix}\\
J(\theta) &= \frac{1}{m} \times - y^Tlog(h) - (1-y)^Tlog(1-h) && \text{The cost function}\\
\theta :&= \theta - \frac{\alpha}{m} X^T (h - y) && \text{Gradient Descent}
\end{align*}
#+end_export
* Cost Function
- Training set: Pairs of inputs and their corresponding outputs
- We can look at the cost function as: \(\text{Cost}(h_{\theta}(x),y)\)
- If we use the same cost function (1/2 mean squared error) as in linear regression then it could be "non-convex"
  - non-convex means there are many local optima
- For logistic regression we will use:
#+begin_export latex
\begin{equation}
\text{Cost}(h_{\theta}(x),y) =
\begin{cases}
-log(h_{\theta}(x)) &\text{ if } y = 1\\
-log(1 - h_{\theta}(x)) &\text{ if } y = 0
\end{cases}
\end{equation}
#+end_export
- Captures intuition that if \(h_{\theta}(x) = 0\) but we know that \(y = 1\) we will penalize learning algorithm by a very large cost
  - Like the probability is zero
  - Graph of \(-log(z)\) for \(0 \le z \le 1\)
* Decision Boundary
- The decision boundary is found in terms of the \(\theta\) values by rearranging to an inequality
- For example: \(\theta_1 = -1, \theta_2 = 3, \theta_3 = 3\) can be arranged to \(3x_1 + 3x_2 \ge 1\)
