#+TITLE: SVM Kernels
#+DATE: <2020-10-06 Tue 14:31>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: svm_kernels
#+ROAM_TAGS: SVM

#+begin_export latex
\begin{equation}
f_i = \text{similarity}(x, l^{(i)}) = e^{-\frac{\| x - l^{(i)} \|}{2 \sigma^2}}
\end{equation}
#+end_export
- The similarity function is called the "Kernel" (in this case a gaussian kernel)
- If \(x \approx l^{(i)}\) then \(f_1\) will be approximately 1
- If \(x\) far from \(l^{(i)}\) then \(f_1\) will be approximately 0
- \(sigma^2\) is a parameter to the kernel function
* Choosing Landmarks
- Given \(m\) training examples, set the landmarks \(l^{(i)}\) to be the locations of the training example \(x^{(i)}\)
* SVM with Kernels
- Replace \(\Theta^T x^{(i)}\) with \(\Theta^T f^{(i)}\)
* SVM Parameters
- \(C ( = \frac{1}{\lambda})\)
  - Large C: Lower bias, high variance
  - Small C: Higher bias, low variance
- \(\sigma^2\)
  - Large: Features \(f_i\) vary more smoothly
    - Higher bias and lower variance
  - Small: Features vary less smoothly
    - Lower bias, higher variance
