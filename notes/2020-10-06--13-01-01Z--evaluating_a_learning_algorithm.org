#+TITLE: Evaluating a Learning Algorithm
#+DATE: <2020-10-06 Tue 14:01>
#+SETUPFILE: ./hugosetup.org
#+HUGO_SLUG: evaluating_a_learning_algorithm
#+ROAM_TAGS: Machine_Learning

* Some options when a hypothesis make large errors in its predictions
- More training example
- Try smaller sets of features
- Try getting additional features
- Try adding polynomial features
- Try decreasing \(\lambda\)
- Try increasing \(\lambda\)
- *Unfortunately the main strategy for choosing is random*
* Evaluating a Hypothesis
- Split the training data into (typically around 70/30):
  - Training Set
  - Test Set
- If there is any ordering then use a random split
- Learn the parameter \(\theta\) from the training data
- Then compute the test set error \(J_{\text{test}}(\theta)\)
- *Miss-Classification Error*:
  - If the hypothesis was correct assign it a 1
  - Otherwise a 0
  - Then divide the sum of all these by the number of test set examples and you have a fraction representing the number of incorrect predictions
* Model Selection and Train/Validation/Test Sets
- Split the data into 3 pieces:
  - Training Set: 60%
  - Cross Validation Set: 20%
  - Test Set: 20%
#+begin_export latex
\begin{align}
J_{\text{train}}(\theta) &= \frac{1}{2m} \sum_{i=1}^{m}
 (h_{\theta}(x^{(i)})-y^{(i)})^2 &\text{Training Error} \\
J_{\text{cv}}(\theta) &= \frac{1}{2m_{\text{cv}}} \sum_{i=1}^{m_{\text{cv}}}
(h_{\theta}(x_{\text{cv}}^{(i)})-y_{\text{cv}}^{(i)})^2 &\text{Cross Validation Error} \\
J_{\text{cv}}(\theta) &= \frac{1}{2m_{\text{test}}} \sum_{i=1}^{m_{\text{test}}} (h_{\theta}(x_{\text{test}}^{(i)})-y_{\text{test}}^{(i)})^2 &\text{Test Error}
\end{align}
#+end_export
- Use cross validation set to test hypotheses to determine best degree of polynomial
- Then use test set to test for generalisation
* [[file:2020-10-06--13-23-41Z--handling_skewed_data.org][Handling Skewed Data]]
