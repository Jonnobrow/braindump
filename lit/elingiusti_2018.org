#+setupfile: ~/.config/emacs/org-templates/roam-note.org
#+title: Elingiusti_2018: Pdf-malware detection: a survey and taxonomy of current techniques
#+roam_key: cite:Elingiusti_2018
- tags :: [[file:../2020-10-04--17-26-01Z--pdf.org][PDF]] [[file:../2020-10-01--19-58-02Z--malware_detection.org][Malware detection]]

* Pdf-malware detection: a survey and taxonomy of current techniques
  :PROPERTIES:
  :Custom_ID: Elingiusti_2018
  :URL: http://dx.doi.org/10.1007/978-3-319-73951-9_9
  :AUTHOR: Elingiusti, M., Aniello, L., Querzoni, L., & Baldoni, R.
  :NOTER_DOCUMENT: ../../pdfs/Elingiusti_2018.pdf
  :NOTER_PAGE: 3
  :END:

  - Discussion of many feature extraction techniques relating to metadata, JavaScript and whole file analysis
  - "no system has explored, so far, the usage of machine learning classification techniques in combination with OpCode sequences as possible features"
  - Mention of clear gaps in the current state of the art that may be interesting for future research

** 1 Introduction
:PROPERTIES:
:NOTER_PAGE: (1 . 0.581175)
:END:

*** Succinct Definition of PDF
:PROPERTIES:
:NOTER_PAGE: (1 . 0.6972624798711755)
:END:

*** Office Macros vs PDF
:PROPERTIES:
:NOTER_PAGE: (2 . 0.1529790660225443)
:END:
- Traps hidden in Microsoft Office macros are increasingly known about - but not PDF

** 2 Background on Malicious PDF Files
:PROPERTIES:
:NOTER_PAGE: (3 . 0.408542)
:END:

*** 2.1 The [[file:../2020-10-04--17-26-01Z--pdf.org][Portable Document Format]]
:PROPERTIES:
:NOTER_PAGE: (3 . 0.590182)
:END:

*** 2.2 PDF Document Obfuscation Techniques
:PROPERTIES:
:NOTER_PAGE: (6 . 0.079787)
:END:

- Split malicious code between objects
- White space randomisation (prevents simple signature matching systems)
- Comment randomisation
- Variable name randomisation
- String, Function and Integer Obfuscation
- Representing data differently before runtime and reassembly at runtime
   - "extremely powerful against static analysis, while it is potentially subject to detection with dynamic analysis"



** 3 Taxonomy of PDF Malware Detection Approaches
:PROPERTIES:
:NOTER_PAGE: (7 . 0.26443)
:END:

*** 3.1 Features
:PROPERTIES:
:NOTER_PAGE: (7 . 0.713278)
:END:

**** 3.1.1 Metadata
:PROPERTIES:
:NOTER_PAGE: (9 . 0.165353)
:END:

**** 3.1.2 JavaScript
:PROPERTIES:
:NOTER_PAGE: (10 . 0.309465)
:END:

**** 3.1.3 Whole File
:PROPERTIES:
:NOTER_PAGE: (12 . 0.644224)
:END:

**** 3.1.4 Feature Selection
:PROPERTIES:
:NOTER_PAGE: (13 . 0.258425)
:END:

*** 3.2 Detection Approaches
:PROPERTIES:
:NOTER_PAGE: (13 . 0.665241)
:END:

**** 3.2.1 Statistical Analysis
:PROPERTIES:
:NOTER_PAGE: (14 . 0.180365)
:END:

**** 3.2.2 Machine Learning Classification
:PROPERTIES:
:NOTER_PAGE: (14 . 0.417549)
:END:

**** 3.2.3 Clustering
:PROPERTIES:
:NOTER_PAGE: (14 . 0.797343)
:END:

**** 3.2.4 Signature Matching
:PROPERTIES:
:NOTER_PAGE: (16 . 0.635217)
:END:

** 4 State of the Art Discussion
:PROPERTIES:
:NOTER_PAGE: (18 . 0.079787)
:END:

*** 4.1 Related Works
:PROPERTIES:
:NOTER_PAGE: (20 . 0.079787)
:END:

** 5 Conclusions
:PROPERTIES:
:NOTER_PAGE: (21 . 0.282444)
:END:

** References
:PROPERTIES:
:NOTER_PAGE: (21 . 0.65173)
:END:
